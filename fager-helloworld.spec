Name:       {{{ git_dir_name }}}
Version:    {{{ git_dir_version }}}
Release:    1%{?dist}
Summary:    This is a test package

License:    GPLv2+
URL:        https://gitlab.com/fager/fager-helloworld/
VCS:        {{{ git_dir_vcs }}}

Source:     {{{ git_dir_pack }}}

%description
This is a test package

%build

%prep
{{{ git_dir_setup_macro }}}

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_bindir}
cp fager-helloworld.sh $RPM_BUILD_ROOT/%{_bindir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,775)
%{_bindir}/fager-helloworld.sh

%changelog
{{{ git_dir_changelog }}}

